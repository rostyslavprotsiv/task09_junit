package com.rostyslavprotsiv.view;

import java.util.*;

public final class Menu extends AbstractMenu {
    private int M;
    private int N;
    private double p;

    public Menu() {
        menu.put(0, "Find longest plateau.");
        menu.put(1, "Out generated Minefield.");
        menu.put(2, "Out solved Minefield.");
        menu.put(3, "Exit");
        methodsForMenu.put(0, this::printFoundLongestPlateau);
        methodsForMenu.put(1, this::printGotFieldForOut);
        methodsForMenu.put(2, this::printGotSolvedField);
        methodsForMenu.put(3, this::quit);
    }

    public void show() {
        out();
    }


    public int getM() {
        return M;
    }

    public int getN() {
        return N;
    }

    public double getP() {
        return p;
    }

    public void inputMNP() {
        int rows;
        int columns;
        double p;
        LOGGER.info("Please, input the number of rows : ");
        if (scan.hasNextInt() && (rows = scan.nextInt()) > 0) {
            LOGGER.info("Please, input the number of columns : ");
            if (scan.hasNextInt() && (columns = scan.nextInt()) > 0) {
                LOGGER.info("Please, input the percentage : ");
                if (scan.hasNextDouble() && (p = scan.nextDouble()) > 0 && p < 100) {
                    M = rows;
                    N = columns;
                    this.p = p;
                } else {
                    badInput();
                }
            } else {
                badInput();
                String logMessage = "Used incorrect size of columns";
                LOGGER.info(logMessage);
                LOGGER.warn(logMessage);
                LOGGER.error(logMessage);
                LOGGER.fatal(logMessage);
            }
        } else {
            badInput();
        }
    }

    public void printGotFieldForOut() {
        inputMNP();
        outMineField(CONTROLLER.getFieldForOut(M, N, p));
    }

    public void printGotSolvedField() {
        outMineField(CONTROLLER.getSolvedField());
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task09_JUNIT");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printFoundLongestPlateau() {
        LOGGER.info("Please, input array of values : ");
        List<Integer> inputtedArray = new LinkedList<>();
        int[] arr;
        int inputted;
        do {
            scan = new Scanner(System.in, "UTF-8");
            if (scan.hasNextInt()) {
                inputted = scan.nextInt();
                inputtedArray.add(inputted);
            } else {
                break;
            }
        } while (true);
        arr = new int[inputtedArray.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = inputtedArray.get(i);
        }
        LOGGER.info(Arrays.toString(CONTROLLER.findLongestPlateau((arr))));
    }

    private void outMineField(char[][] field) {
        StringBuilder topBound = new StringBuilder("_");
        StringBuilder bound = new StringBuilder("|");
        for (int i = 0; i < field.length; i++) {
            topBound.append("__");
            bound.append("_|");
        }
        LOGGER.info(topBound);
        for (int i = 0; i < field.length; i++) {
            StringBuilder oneStr = new StringBuilder();
            oneStr.append("|");
            for (int j = 0; j < field[i].length; j++) {
                oneStr.append(field[i][j] + "|");
            }
            LOGGER.info(oneStr);
            LOGGER.info(bound);
        }
    }
}
