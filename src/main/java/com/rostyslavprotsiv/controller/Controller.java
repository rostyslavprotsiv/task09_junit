package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.LongestPlateauAction;
import com.rostyslavprotsiv.model.action.MinesweeperAction;
import com.rostyslavprotsiv.model.creator.MinefieldGeneratorImpl;
import com.rostyslavprotsiv.view.AbstractMenu;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final LongestPlateauAction LONGEST_PLATEAU_ACTION
            = new LongestPlateauAction();
    private final MinefieldGeneratorImpl GENERATOR
            = new MinefieldGeneratorImpl();
    private final MinesweeperAction MINESWEEPER_ACTION
            = new MinesweeperAction(GENERATOR);
    private char[][] generatedField;
    private Menu MENU;

    public Controller(AbstractMenu menu) {
        MENU = (Menu) menu;
    }

    public Controller() {
    }

    public int[] findLongestPlateau(int... array) {
        return LONGEST_PLATEAU_ACTION.findLongestPlateau(array);
    }

    public char[][] getFieldForOut(int M, int N, double p) {
        return generatedField = MINESWEEPER_ACTION.getMassForOutput(M, N, p);
    }

    public char[][] getSolvedField() {
        if (generatedField == null) {
            String logMessage = "You didn't input values before";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            MENU.inputMNP();
            return MINESWEEPER_ACTION.getSolvedField(MINESWEEPER_ACTION
                    .getMassForOutput(MENU.getM(), MENU.getN(), MENU.getP()));
        }
        return MINESWEEPER_ACTION.getSolvedField(generatedField);
    }
}
