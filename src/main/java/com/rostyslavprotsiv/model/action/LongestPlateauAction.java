package com.rostyslavprotsiv.model.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class LongestPlateauAction {
    private final Logger LOGGER = LogManager.getLogger(LongestPlateauAction.class);

    public int[] findLongestPlateau(int... array) {
        int finalStart = 0;
        int finalEnd = 0;
        int maxLength = 0;
        List<Integer> starts = new LinkedList<>();
        LinkedList<Integer> ends = new LinkedList<>();
        fillListsOfPositions(starts, ends, array);
        /*
         * To maxLength added 1 because we always include end and start elements
         * so maxLength without this will be less on 1.
         */
        for (int i = 0; i < starts.size(); i++) {
            if (ends.get(i) - starts.get(i) + 1 >= maxLength) {
                maxLength = ends.get(i) - starts.get(i) + 1;
                finalEnd = ends.get(i);
                finalStart = starts.get(i);
            }
        }
        if (maxLength == 0) {
            String logMessage = "Used incorrect mass for finding";
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        }
        return new int[]{maxLength, finalStart, finalEnd};
    }

    private void fillListsOfPositions(List<Integer> starts, List<Integer> ends,
                                      int... array) {
        boolean hasFirst = false;
        for (int i = 1; i < array.length - 1; i++) {
            if (!hasFirst && array[i] == array[i + 1] && array[i - 1] < array[i]) {
                starts.add(i);
                hasFirst = true;
            } else if (hasFirst && array[i] != array[i + 1]) {
                hasFirst = false;
                if (array[i] > array[i + 1]) {
                    ends.add(i);
                } else {
                    ((LinkedList<Integer>) starts).removeLast();
                }
            }
        }
    }
}
