package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.creator.IMinefieldCreator;

public class MinesweeperAction {
    private IMinefieldCreator creator;

    public MinesweeperAction() {
    }

    public MinesweeperAction(IMinefieldCreator creator) {
        this.creator = creator;
    }

    public char[][] getMassForOutput(int M, int N, double p) {
        boolean[][] boolMineField = creator.generateBooleanArr(M, N, p);
        char[][] mineField =
                new char[boolMineField.length][boolMineField[0].length];
        for (int i = 0; i < mineField.length; i++) {
            for (int j = 0; j < mineField[i].length; j++) {
                mineField[i][j] = boolMineField[i][j] ? '*' : ' ';
            }
        }
        return mineField;
    }

    public char[][] getSolvedField(char[][] mineField) {
        int tempValue;
        for (int i = 1; i < mineField.length - 1; i++) {
            for (int j = 1; j < mineField[i].length - 1; j++) {
                if (mineField[i][j] == ' ') {
                    tempValue = scanSquare(i, j, mineField);
                    mineField[i][j] = tempValue == 0 ? ' ' :
                            Integer.toString(tempValue)
                                    .charAt(0);
                }
            }
        }
        return mineField;
    }

    private int scanSquare(int row, int column, char[][] mineField) {
        int value = 0;
        for (int i = row - 1; i < row + 2; i++) {
            for (int j = column - 1; j < column + 2; j++) {
                if (mineField[i][j] == '*') {
                    value++;
                }
            }
        }
        return value;
    }
}
