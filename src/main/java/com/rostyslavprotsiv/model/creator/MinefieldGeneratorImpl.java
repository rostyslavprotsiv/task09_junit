package com.rostyslavprotsiv.model.creator;

import static java.lang.Math.*;

public class MinefieldGeneratorImpl implements IMinefieldCreator {

    @Override
    public boolean[][] generateBooleanArr(int M, int N, double p) {
        boolean[][] mineField;
        if (checkMN(M, N) && checkP(p)) {
            mineField = new boolean[M + 2][N + 2];//2 is additional space
            for (int i = 1; i < mineField.length - 1; i++) {
                for (int j = 1; j < mineField[i].length - 1; j++) {
                    mineField[i][j] = random() < p / 100;
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
        return mineField;
    }

    private boolean checkP(double p) {
        return p > 0 && p < 100;
    }

    private boolean checkMN(int M, int N) {
        return M > 0 && N > 0;
    }
}
