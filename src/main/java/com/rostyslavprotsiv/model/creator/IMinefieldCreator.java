package com.rostyslavprotsiv.model.creator;

public interface IMinefieldCreator {
    boolean[][] generateBooleanArr(int M, int N, double p);
}
