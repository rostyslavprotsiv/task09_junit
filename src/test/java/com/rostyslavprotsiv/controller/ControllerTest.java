package com.rostyslavprotsiv.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ControllerTest {
    protected int[] inputtedArray = new int[]{11, 12, 13, 13, 13, 15, 16, 16,
            16, 16, 15, 18, 19, 19, 19, 19, 111};

    @Test
    public void testFindLongestPlateau() {
        Controller controller = new Controller();
        assertArrayEquals(controller.findLongestPlateau(inputtedArray),
                new int[]{4, 6, 9});
    }
}
