package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.creator.IMinefieldCreator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MinesweeperActionTest {
    protected final Logger LOGGER =
            LogManager.getLogger(MinesweeperAction.class);
    private static final boolean[][] boolMineField =
            new boolean[][]{
                    {false, false, false, false, false, false, false, false},
                    {false, true, false, false, false, false, true, false},
                    {false, true, false, true, false, false, false, false},
                    {false, false, false, false, false, false, false, false},
                    {false, false, false, false, false, false, false, false},
                    {false, false, false, false, false, false, true, false},
                    {false, false, true, false, true, true, false, false},
                    {false, false, false, false, false, false, false, false}};
    private static final char[][] mineField =
            new char[][]{
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                    {' ', '*', ' ', ' ', ' ', ' ', '*', ' '},
                    {' ', '*', ' ', '*', ' ', ' ', ' ', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' ', '*', ' '},
                    {' ', ' ', '*', ' ', '*', '*', ' ', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}};

    private static final char[][] solvedMineField =
            new char[][]{
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                    {' ', '*', '3', '1', '1', '1', '*', ' '},
                    {' ', '*', '3', '*', '1', '1', '1', ' '},
                    {' ', '1', '2', '1', '1', ' ', ' ', ' '},
                    {' ', ' ', ' ', ' ', ' ', '1', '1', ' '},
                    {' ', '1', '1', '2', '2', '3', '*', ' '},
                    {' ', '1', '*', '2', '*', '*', '2', ' '},
                    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}};

    @InjectMocks
    static MinesweeperAction minesweeperAction;

    @Mock
    IMinefieldCreator creator;

    @Test
    public void testGetMassForOutput() {
        when(creator.generateBooleanArr(6, 6, 12.5)).thenReturn(
                boolMineField);
        assumeTrue(Arrays.deepEquals(minesweeperAction.getMassForOutput(6, 6,
                12.5),
                mineField));
        outMineField();
    }

    @Test
    public void testGetSolvedField() {
        minesweeperAction.getSolvedField(mineField);
        assumeTrue(Arrays.deepEquals(mineField, solvedMineField));
        outMineField();
    }

    private void outMineField() {
        LOGGER.info("_________________");
        for (int i = 0; i < mineField.length; i++) {
            StringBuilder oneStr = new StringBuilder();
            oneStr.append("|");
            for (int j = 0; j < mineField[i].length; j++) {
                oneStr.append(mineField[i][j] + "|");
            }
            LOGGER.info(oneStr);
            oneStr = new StringBuilder();
            LOGGER.info("|_|_|_|_|_|_|_|_|");
        }
    }
}
