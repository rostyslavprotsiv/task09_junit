package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LongestPlateauActionTest {
    private static final int[] TEST_ARR = new int[]{6, 5, 1, 2, 2, 2, 2, 2, 4,
            5, 5, 5, 5, 3};
    private static final int[] TEST_ARR1 = new int[]{1, 2, 2, 1};
    private static final int[] TEST_ARR2 = new int[]{1, 2, 1};
    private LongestPlateauAction action = new LongestPlateauAction();

    @RepeatedTest(3)
    void testFindLongestPlateau() {
        assertTrue(Arrays.equals(action.findLongestPlateau(TEST_ARR),
                new int[]{4, 9, 12}));
    }

    @BeforeAll
    public static void test1FindLongestPlateau() {
        LongestPlateauAction action = new LongestPlateauAction();
        assertTrue(Arrays.equals(action.findLongestPlateau(TEST_ARR1),
                new int[]{2, 1, 2}));
    }

    @AfterAll
    public static void test2FindLongestPlateau() {
        LongestPlateauAction action = new LongestPlateauAction();
        assertTrue(Arrays.equals(action.findLongestPlateau(TEST_ARR2),
                new int[]{0, 0, 0}));
    }
}
