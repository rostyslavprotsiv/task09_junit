package com.rostyslavprotsiv.model.creator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MinefieldGeneratorImplTest {
    @Test
    public void testGeneratorBooleanArr() {
        MinefieldGeneratorImpl generator = new MinefieldGeneratorImpl();
        boolean[][] generated = generator.generateBooleanArr(6, 6, 20);
        int numOfStars = 0;
        assertEquals(generated.length, 8);
        assertEquals(generated[0].length, 8);
        for (int i = 0; i < generated.length; i++) {
            for (int j = 0; j < generated[i].length; j++) {
                if (generated[i][j]) {
                    numOfStars += 1;
                }
            }
        }
        assertTrue(numOfStars <= 36 * 0.2 + 4
                && numOfStars >= 36 * 0.2 - 4);
    }
}
